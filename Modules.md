# Modules

## Common/Public ##

Modules that used for public/common functionality

* [Script Blocker](Modules/Script Blocker)
* GEO IP


## Admin ##

Modules that used for admin functionality

* Policy generator
* Themes
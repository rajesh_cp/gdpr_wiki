# Class Reference

The main class files included in this plugin.

## Classes 

[Cookie_Law_Info](Class Reference/Cookie_Law_Info)

[Cookie_Law_Info_i18n](Class Reference/Cookie_Law_Info_i18n)

[public/Cookie_Law_Info_Public](Class Reference/Cookie_Law_Info_Public)

[admin/Cookie_Law_Info_Admin](Class Reference/Cookie_Law_Info_Admin)

[third-party/Cookie_Law_Info_Third_Party](Class Reference/Cookie_Law_Info_Third_Party)
= Cookie_Law_Info =

This is the main class file. Which initiates the Admin, Public, Third party hooks. And also loads translation.

== Methods summary ==


**get_settings** \\ 
Get current settings.
{{{
#!php
get_settings()
}}}

| **Returns** | //array// settings

\\ 
\\ 
**generate_settings_tabhead** \\ 
Generate tab head HTML for settings page.
{{{
#!php
generate_settings_tabhead(array $title_arr)
}}}
| **Parameters** | //array// $title_arr - Titles for settings tabs |
|**Output**| Prints Tab head HTML |